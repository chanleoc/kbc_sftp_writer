"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment (unicode script fixes in place)
"""
import pip
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'paramiko'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'pygelf'])

#import pysftp
import paramiko
import os
import sys
from pygelf import GelfTcpHandler
import pandas as pd
import csv
import logging
import math
from keboola import docker

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

# get proper list of tables
DEFAULT_FILE_DESTINATION = "/data/in/tables/"
DEFAULT_OUTPUT_DESTINATION = "/data/out/tables/"

"""
Configuration
{
    "sftp_url":"sftp.stg-us-east.medispend.com",
    "destination":"/outbound/",
    "user":"kbc",
    "#password":"",
    "filesize":100,
    "table_columns":[
        "Organization8Type","Inclusion7Exclusion8Criteria","Institution8Name",
        "Tax8ID","NPI","CR_cId","Address1AddressLine1","Address1AddressLine2",
        "Address1City","Address1State","Address1Country","Address1Zip",
        "Last8Update","Session8ID"
    ],
    "desired_headers":[]
}
"""

# Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
SFTP_URL = cfg.get_parameters()["sftp_url"]
DESTINATION = cfg.get_parameters()["destination"]
USER = cfg.get_parameters()["user"]
PASSWORD = cfg.get_parameters()["#password"]
FILESIZE = cfg.get_parameters()["filesize"]
COLUMN_HEADERS = cfg.get_parameters()["table_columns"]
DESIRED_HEADERS = cfg.get_parameters()["desired_headers"]

# Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
logging.info("IN tables mapped: "+str(in_tables))

def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name

def header_replace(target,find, replace):
    """ replace headers: "test file" --> "test_file" (if needed) """
    """ Replace all the "find" in target with "replcae"."""
    result = target.replace(find,replace)
    return result

def break_up(df,start,end,limit,file_name):
    """break up into multiple files"""
    """start = where to start in file; end = the number of rows in file"""
    """df = the dataframe; limit = number of rows to output per file"""
    number_of_files = math.ceil(len(df)/limit)
    itr = 0
    current_row = 0

    file_list=[]
    while itr<number_of_files:
        if itr+1==number_of_files:
        # the last file
            temp_df = df[start:end]
            temp_file_name = file_name+"_"+str(itr+1)+".csv"
            file_list.append(temp_file_name)
            if len(DESIRED_HEADERS) >=1:
                temp_df.to_csv(DEFAULT_FILE_DESTINATION+temp_file_name,columns=DESIRED_HEADERS,index=False)
            else:
                temp_df.to_csv(DEFAULT_FILE_DESTINATION+temp_file_name,columns=COLUMN_HEADERS_MOD,index=False)
        else:
            temp_df = df[start:start+limit]
            temp_file_name = file_name+"_"+str(itr+1)+".csv"
            file_list.append(temp_file_name)
            if len(DESIRED_HEADERS) >=1:
                temp_df.to_csv(DEFAULT_FILE_DESTINATION+temp_file_name,columns=DESIRED_HEADERS,index=False)
            else:
                temp_df.to_csv(DEFAULT_FILE_DESTINATION+temp_file_name,columns=COLUMN_HEADERS_MOD,index=False)
            start += limit
        itr+=1
    return file_list


if __name__ == "__main__":
    """
    Main execution script.
    """
    
    ### SFTP Connection ###
    port = 22  
    conn = paramiko.Transport((SFTP_URL,port))
    conn.connect(username=USER,password=PASSWORD)
    sftp = paramiko.SFTPClient.from_transport(conn)
    #print(sftp.listdir())

    in_file = get_tables(in_tables)
    in_file_name = (in_file.split("/")[4]).split(".")[0]
    temp_name = "temp"

    COLUMN_HEADERS_MOD = []
    for i in COLUMN_HEADERS:
        temp = header_replace(i,"8"," ")
        temp = header_replace(temp,"7","/")
        COLUMN_HEADERS_MOD.append(temp)
    logging.info("Modified Header: ["+','.join(COLUMN_HEADERS_MOD)+"]")
    data = pd.read_csv(in_file,dtype=str)
    data_df = pd.DataFrame(data,columns=COLUMN_HEADERS)
    data_df.columns = COLUMN_HEADERS_MOD
    if len(DESIRED_HEADERS) >= 1:
        logging.info("Desired HEADER: ["+','.join(DESIRED_HEADERS)+"]")
    
    if FILESIZE == "":
        if len(DESIRED_HEADERS) >= 1:
            data_df.to_csv(DEFAULT_FILE_DESTINATION+in_file_name+".csv",columns=DESIRED_HEADERS,index=False)
        else:
            data_df.to_csv(DEFAULT_FILE_DESTINATION+in_file_name+".csv",columns=COLUMN_HEADERS_MOD,index=False)
        source = DEFAULT_FILE_DESTINATION+in_file_name+".csv"
        destination = "/outbound/"+in_file_name+".csv"
        logging.info("File Source: " + source)
        logging.info("File Destination: "+destination)
        sftp.put(source,destination)
    else:
        file_list = break_up(data_df,0,len(data_df),FILESIZE,in_file_name)
        for i in file_list:
            source = DEFAULT_FILE_DESTINATION+i
            destination = "/outbound/"+i
            sftp.put(source,destination)
            logging.info("File Source: " + source)
            logging.info("File Destination: "+destination)

    sftp.close()
    conn.close()
    print("Done.")
